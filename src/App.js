import {Component} from "react";
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
    state={openModal1:false,openModal2:false}

handleClick = (modalId) => {
        this.setState({[modalId]:!this.state[modalId]})
    }
    render() {
        return <div className="App"><Button text={"Button1"} bgColor={"red"} click={() => this.handleClick("openModal1")}/>
            {this.state.openModal1&&<Modal click={() => this.handleClick("openModal1")}  header={"Modal1"} closeButton={true} text={"Hello, Max!"}
                   actions={<div className={"button-wrapper"}><Button text={"Ok"} bgColor={"green"}
                                      click={() => {
                                          console.log("ok")
                                      }}/><Button text={"Cancel"}
                                                  bgColor={"green"}
                                                  click={() => this.handleClick("openModal1")}/></div>}/>}
            <Button text={"Button2"} bgColor={"blue"} click={() => this.handleClick("openModal2")}/>
            {this.state.openModal2&&<Modal click={() => this.handleClick("openModal2")} header={"Modal2"} closeButton={false} text={"By, Max !"}
                                           actions={<div className={"button-wrapper"}><Button text={"Ok"} bgColor={"green"}
                                                              click={() => {
                                                                  console.log("ok")
                                                              }}/><Button text={"Cancel"}
                                                                          bgColor={"blue"}
                                                                          click={() => this.handleClick("openModal2")}/></div>}/>}
        </div>

    }
}

export default App;
