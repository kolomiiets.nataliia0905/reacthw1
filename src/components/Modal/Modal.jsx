import {Component} from "react";
import './Modal.module.css';
import Button from "../Button/Button";
import styles from "./Modal.module.css"
class Modal extends Component {
    render() {
        const {header, closeButton, text, actions, click} = this.props
        return <div className={styles.wrapper} onClick={click}>
            <div className={styles.modal} onClick={(e) => {e.stopPropagation()}}>
                <h2>{header}</h2>
                {closeButton && <Button className={styles.closeBtn} text={"X"}
                                        click={click}/>}
                <p>{text}</p>
                {actions}

            </div>
        </div>
    }
}

export default Modal;