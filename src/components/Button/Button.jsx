import {Component} from "react";
import styles from './Button.module.css';
class Button extends  Component {
    render () {
        const {text, bgColor, click, className=""} = this.props
        return <button style = {{background : bgColor}} className={className} onClick={click}>{text}</button>
    }
}

export default Button;
